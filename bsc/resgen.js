var redis = require('redis');

var bluebird = require("bluebird");

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var client = redis.createClient();
client.on('connect', function() {
    console.log('connected');
});
var dateToInt = function(date) {
    return Math.floor(date / (1000 * 60 * 60 * 24));
};

var intToDate = function(num) {
    num = num * 1000 * 60 * 60 * 24;
    return new Date(num);
}

var date = dateToInt(new Date());
var days = [];
while (date < dateToInt(new Date(2016, 11, 31))) {
    date += 1;

    // console.log(date);
    // console.log(intToDate(date));
    days.push(date);
}
// console.log(days);
// var roomtypes;
// client.lrangeAsync(["roomtypes", 0, 100]).then(function(res, err) {
//     console.log("res:", res);
//     res.forEach(function(item, index) {
//         client.smembersAsync([item]).then(function(res, err) {
//             console.log(item, res);
//             days.forEach(function(day, num) {
//               client.sadd(day, res) 
//             });
//             console.log("rooms:", item, res);
//         });
//         client.expire(item, 86400 * (index + 1));
//     })
// });


// Generating room types //

client.lrange(["roomtypes", "0", "100"],  function(err, res) {
    res.forEach(function(item, index) {
        console.log(item);
        for(i = 1 + index * 25; i <= index * 25 + 25; i++) {
            console.log(i);
            client.sadd(item, i);
        }
    })
});

// var roomtypes;
// client.lrangeAsync(["roomtypes", 0, 100]).then(function(res, err) {
//     console.log("res:", res);
//     res.forEach(function(item, index) {
//         client.smembersAsync([item]).then(function(res, err) {
//             days.forEach(function(day, num) {
//               client.del(day);
//             });
//             console.log("rooms:", item, res);
//         });
//         client.expire(item, 86400 * (index + 1));
//     })
// });

// client.keys(['1*'], function(err, res) {
//     client.del(res);
// })
// client.keys(['2*'], function(err, res) {
//     client.del(res);
// })
// client.keys(['3*'], function(err, res) {
//     client.del(res);
// })
// client.keys(['4*'], function(err, res) {
//     client.del(res);
// })
// client.keys(['5*'], function(err, res) {
//     client.del(res);
// })
// client.keys(['6*'], function(err, res) {
//     client.del(res);
// })
// client.keys(['7*'], function(err, res) {
//     client.del(res);
// })
// client.keys(['8*'], function(err, res) {
//     client.del(res);
// })
// client.keys(['9*'], function(err, res) {
//     client.del(res);
// })
