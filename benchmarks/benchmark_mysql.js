var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'sandor_ti',
    password: '',
    database: 'c9'
});

connection.connect();
console.log("conn!");
console.time("db_write");
connection.query("DROP TABLE Stuff");
connection.query('CREATE TABLE Stuff (thing TEXT)', function(err, rows, fields) {
   if (!err)
     console.log('ok: ', rows);
   else
     console.log('Error while performing Query.');
});
var rnd;
  
for (var i = 0; i < 3000; i++) {
  rnd = Math.floor(Math.random() * 10000000);
  connection.query("INSERT INTO Stuff VALUES (" + rnd + ")", function(err, res){
      if(err) {
          console.log(err);
      } else {
          //console.log(res);
      }
  })
}
console.time("Read");
connection.query("SELECT * FROM Stuff", function(err, rows, fields) {
    console.log(rows);
    
})
 
connection.end(function() {
    console.timeEnd("Read");
    console.timeEnd("db_write");
});