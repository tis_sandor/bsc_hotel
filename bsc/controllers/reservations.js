module.exports.controller = function(app) {

    app.get('/reservations', function(req, res) {
        if (req.session.email === undefined) {
            res.redirect('/login');
        }
        var redis = require('redis');
        var client = redis.createClient();
        client.on('connect', function() {
            client.lrange(['roomtypes', 0, 100], function(err, roomtypes) {
                client.keys('R:*' + req.session.email + '*', function(err, rsv) {
                    console.log("Reservations:", rsv);
                    res.render('reservations', {
                        title: 'Express',
                        user: req.session.email,
                        roomtypes: roomtypes,
                        reservations: rsv
                    });
                });
            });
        });
    });

    app.get('/reservations/cancel/:reservation_id', function(req, res) {
        console.log("Res_ID:", req.params.reservation_id);
        var redis = require('redis');
        var client = redis.createClient();
        client.on('connect', function() {
            var reserv = require("../models/reservations.js");
            var instance = new reserv(client);
            instance.cancelReservation(req.params.reservation_id, function(){
                var backURL=req.header('Referer');
                res.redirect(backURL);
            });
            
        });
    });
    app.get('/reservations/activate/:reservation_id', function(req, res) {
        console.log("Res_ID:", req.params.reservation_id);
        var redis = require('redis');
        var client = redis.createClient();
        client.on('connect', function() {
            var reserv = require("../models/reservations.js");
            var instance = new reserv(client);
            instance.activateReservation(req.params.reservation_id, function(){
                res.redirect('/admin_active');
            });
            
        });
    });
    app.get('/reservations/close/:reservation_id', function(req, res) {
        console.log("Res_ID:", req.params.reservation_id);
        var redis = require('redis');
        var client = redis.createClient();
        client.on('connect', function() {
            var reserv = require("../models/reservations.js");
            var instance = new reserv(client);
            instance.closeReservation(req.params.reservation_id, function(){
                res.redirect('/admin_active');
            });
            
        });
    });
    app.post('/reservations', function(req, res) {
        var redis = require('redis');
        var client = redis.createClient();
        var ok = true;
        client.on('connect', function() {
            console.log('connected');
            console.log(req.body);
            var reserv = require("../models/reservations.js");
            var instance = new reserv(client);
            console.log("reservations imported");
            console.log("reservations instantiated");
            instance.roomsInRange(req.body["from-date"], req.body["to-date"], req.body['roomtype'], function(err, result) {
                if (result[0] === undefined) {
                    console.log('rooms in range: ', result);
                    ok=false;
                     res.render('error', {
                            title: 'Express',
                            user: req.session.email,
                            err: 'We are sorry, but there is no ' + req.body['roomtype'] + ' room available for the selected period!'
                        });
                } else {
                    instance.reserveRoom(req.body["from-date"], req.body["to-date"], result[0], req.body['roomtype'], req.session.email, function(err, result) {
                    console.log(err);
                    console.log('tranzaction result:', result);
                    console.log("reserved!");
                    if (result.length <=1) {
                        ok = false;
                    }
                    if (ok) {
                        res.redirect('/reservations');
                    }
                    else {
                        res.render('error', {
                            title: 'Express',
                            user: req.session.email,
                            err: 'We are sorry, but there is no ' + req.body['roomtype'] + ' room available for the selected period!'
                        });
                    }
                });
                }
                
            });
            
            
        });


    });
}