var fs = require("fs");
var file = "test.db";
var exists = fs.existsSync(file);

var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database(file);

console.time("db_write");

db.serialize(function() {

  if(!exists) {
    db.run("DROP TABLE Stuff");
    db.run("CREATE TABLE Stuff (thing TEXT)");
    db.run("Delete from Stuff");
  }
  
var stmt = db.prepare("INSERT INTO Stuff VALUES (?)");

//Insert random data
  var rnd;
  for (var i = 0; i < 3000; i++) {
    rnd = Math.floor(Math.random() * 10000000);
    stmt.run("Thing #" + rnd);
  }
  
stmt.finalize();

db.each("SELECT COUNT(*) FROM STUFF;", function(err, res) {
    console.log(err);
    console.log(res);
    console.timeEnd("db_write");
});

console.time("Read");
db.each("SELECT * FROM Stuff", function(err, row) {
    console.log(row.id + ": " + row.thing);
  });

});
db.close(function(err, res) {
  console.timeEnd("Read");
});
