module.exports.controller = function(app) {
    app.get('/admin', function(req, res) {
        if (req.session.email === undefined) {
            res.redirect('/login');
        }
        var redis = require('redis');
        var client = redis.createClient();
        client.on('connect', function() {
            var reserv = require("../models/reservations.js");
            var instance = new reserv(client);
            instance.getApStats(function(ap) {
                instance.getSingleStats(function(single) {
                    instance.getCoupleStats(function(couple) {
                        instance.getDoubleStats(function(double) {
                            instance.getYearStats(function(cal_s, cal_d, cal_c, cal_a) {
                                res.render('admin', {
                                    title: 'Express',
                                    user: req.session.user,
                                    ap_used: 25 - ap.length,
                                    ap_free: ap.length,
                                    sg_used: 25 - single.length,
                                    sg_free: single.length,
                                    db_used: 25 - double.length,
                                    db_free: double.length,
                                    cp_used: 25 - couple.length,
                                    cp_free: couple.length, 
                                    cal_a: cal_a, 
                                    cal_s: cal_s,
                                    cal_d: cal_d,
                                    cal_c: cal_c

                                });
                            });
                                
                            
                            
                        })
                    })
                })
                    
            });
        });
        
    });

    app.get('/admin_active', function(req, res) {
        if (req.session.email === undefined) {
            res.redirect('/login');
        }
        var redis = require('redis');
        var client = redis.createClient();
        client.on('connect', function() {
            var reserv = require("../models/reservations.js");
            var instance = new reserv(client);
            instance.getActives(function(err, rsv) {
                res.render('admin_act', {
                    title: 'Express',
                    user: req.session.user, 
                    res: rsv
                });
            });

        });
    });

    app.get('/admin_reservations', function(req, res) {
        if (req.session.email === undefined) {
            res.redirect('/login');
        }
        var redis = require('redis');
        var client = redis.createClient();
        client.on('connect', function() {
            var reserv = require("../models/reservations.js");
            var instance = new reserv(client);
            instance.getReservations(function(err, rsv) {
                console.log(rsv);
                res.render('admin_res', {
                    title: 'Express',
                    user: req.session.user,
                    res: rsv
                });
            });

        });
    });
}