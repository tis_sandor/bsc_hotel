module.exports.controller = function(app) {

    app.get('/login', function(req, res) {
        console.log(req.session);
        res.render('login', {
            title: 'Express',
            user: req.session.email
        });
    });
    
    app.get('/logout', function(req, res) {
        req.session.destroy();
        res.redirect('/');
    });

    app.post('/login', function(req, res) {
        var redis = require('redis');
        var client = redis.createClient();
        client.on('connect', function() {
            console.log(req.session);
            client.hmget(req.body['email'], 'password', function(err, response) {
                if(response[0] === req.body['password']) {
                    req.session.email = req.body['email'];
                    req.session.password = req.body['password'];
                    req.session.save(function(err) {
                        console.log(req.session);
                        res.redirect('/');
                    });
                    
                } else {
                    res.render('error');
                }
            })
        });
    });

    app.get('/register', function(req, res) {
        res.render('register', {
            title: 'Express',
            user: req.session.email
        });
    });

    app.post('/register', function(req, res) {
        var validator = require('validator');
        var redis = require('redis');
        var client = redis.createClient();
        client.on('connect', function() {
            email = req.body['email'];
            if (!validator.isEmail(email) || !req.body['password'] || !req.body['fname'] || !req.body['lname']) {
                res.redirect('error');
            }
            else {
                console.log("email and pass ok");
                client.hmset([email, 'fname', req.body['fname'], 'lname', req.body['lname'], 'password', req.body['password']], function(err, res) {
                    console.log(err);
                    console.log(res);
                })
                res.redirect('/');
            }
        });
    });

}