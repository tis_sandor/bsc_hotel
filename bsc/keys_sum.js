var redis = require('redis');

var bluebird = require("bluebird");

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
var client = redis.createClient();
client.on('connect', function() {
    console.log('connected');
});

var dateToInt = function(date) {
    return Math.floor(date / (1000 * 60 * 60 * 24));
};

var intToDate = function(num) {
    num = num * 1000 * 60 * 60 * 24;
    return new Date(num);
}
console.time("read");
client.keys("*", function(err, res) {
    // console.log(res);
    var cal = {
        jan: 0,
        feb: 0,
        mar: 0,
        apr: 0,
        mai: 0,
        jun: 0,
        jul: 0,
        aug: 0,
        sep: 0,
        oct: 0,
        nov: 0,
        dec: 0,
    }
    console.log(cal);
    var valid_date = dateToInt(new Date(2016, 0, 1))
    var valid_items = [];
    for( var i = 0; i < res.length; i++) {
        var item = parseInt(res[i].substr(2,6));
        if (item >= valid_date) {
            var valid_item = parseInt(res[i].substr(2,6));
            valid_items.push(valid_item);
            valid_item = intToDate(valid_item);
            switch(valid_item.getMonth()) {
                case 0:
                    cal.jan+=1;
                    break;
                case 1: 
                    cal.feb+=1;
                    break;
                case 2: 
                    cal.mar+=1;
                    break;
                case 3: 
                    cal.apr+=1;
                    break;
                case 4: 
                    cal.mai+=1;
                    break;
                case 5: 
                    cal.jun+=1;
                    break
                case 6:
                    cal.jul+=1;
                    break
                case 7:
                    cal.aug+=1;
                    break;
                case 8: 
                    cal.sep+=1;
                    break;
                case 9: 
                    cal.oct+=1;
                    break;
                case 10: 
                    cal.nov+=1;
                    break;
                case 11: 
                    cal.dec+=1;
                    break;
            }
            
            
        }
    }
    console.log(cal);    client.quit;
});

