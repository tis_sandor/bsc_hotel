//on burger click
$('.hamburger').on('click', function() {
    $('.menu').slideToggle();
});

$(".flexslider").flexslider({
    directionNav: false
});

$(window).load(function() {
    $('.flexslider').flexslider();
});

$(document).ready(

    function() {

        $(".graph").niceScroll(".seemerollin", {
            cursorcolor: "#FFF"
        });
        //$(".graph").getNiceScroll().hide();

    }

);

// Load the Visualization API and the corechart package.
google.charts.load('current', {
    'packages': ['corechart']
});

// Set a callback to run when the Google Visualization API is loaded.
google.charts.setOnLoadCallback(drawChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
var chart1 = document.getElementById('chart1');
var free1 = chart1.dataset.free;
var used1 = chart1.dataset.used;
var chart2 = document.getElementById('chart2');
var free2 = chart2.dataset.free;
var used2 = chart2.dataset.used;
var chart3 = document.getElementById('chart3');
var free3 = chart3.dataset.free;
var used3 = chart3.dataset.used;
var chart4 = document.getElementById('chart4');
var free4 = chart4.dataset.free;
var used4 = chart4.dataset.used;

var chart5 = document.getElementById('chart5');
var jan = chart5.dataset.jan.split(',');
var feb = chart5.dataset.feb.split(',');
var mar = chart5.dataset.mar.split(',');
var apr = chart5.dataset.apr.split(',');
var mai = chart5.dataset.mai.split(',');
var jun = chart5.dataset.jun.split(',');
var jul = chart5.dataset.jul.split(',');
var aug = chart5.dataset.aug.split(',');
var sep = chart5.dataset.sep.split(',');
var oct = chart5.dataset.oct.split(',');
var nov = chart5.dataset.nov.split(',');
var dec = chart5.dataset.dec.split(',');
function drawChart() {

    // Create the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Topping');
    data.addColumn('number', 'Slices');
    data.addRows([
        ['Free', parseInt(free1)],
        ['Active', parseInt(used1)],
    ]);

    // Set chart options
    var options = {
        'title': 'Single rooms',
    };

    // Instantiate and draw our chart, passing in some options.
    var chart1 = new google.visualization.PieChart(document.getElementById('chart1'));
    chart1.draw(data, options);
    
    // Create the data table.
    var data2 = new google.visualization.DataTable();
    data2.addColumn('string', 'Topping');
    data2.addColumn('number', 'Slices');
    data2.addRows([
        ['Free', parseInt(free2)],
        ['Active', parseInt(used2)],
    ]);

    // Set chart options
    var options = {
        'title': 'Double rooms',
    };

    // Instantiate and draw our chart, passing in some options.
    var chart2 = new google.visualization.PieChart(document.getElementById('chart2'));
    chart2.draw(data2, options);
    
    // Create the data table.
    var data3 = new google.visualization.DataTable();
    data3.addColumn('string', 'Topping');
    data3.addColumn('number', 'Slices');
    data3.addRows([
        ['Free', parseInt(free3)],
        ['Active', parseInt(used3)],
    ]);

    // Set chart options
    var options = {
        'title': 'Couple rooms',
        hAxis: {
            'title': 'Months'
        },
        vAxis: {
            'title': 'Reservations'
        }
    };

    // Instantiate and draw our chart, passing in some options.
    var chart3 = new google.visualization.PieChart(document.getElementById('chart3'));
    chart3.draw(data3, options);
    
    // Create the data table.
    var data4 = new google.visualization.DataTable();
    data4.addColumn('string', 'Topping');
    data4.addColumn('number', 'Slices');
    data4.addRows([
        ['Free', parseInt(free4)],
        ['Active', parseInt(used4)],
    ]);

    // Set chart options
    var options = {
        'title': 'Apartments',
    };

    // Instantiate and draw our chart, passing in some options.
    var chart4 = new google.visualization.PieChart(document.getElementById('chart4'));
    chart4.draw(data4, options);
    
    var data5 = new google.visualization.DataTable();
    data5.addColumn('string', 'Month');
    data5.addColumn('number', 'Reservations')
    data5.addRows([
        ['January', parseInt(jan)],
        ['February', parseInt(feb)],
        ['March', parseInt(mar)],
        ['April', parseInt(apr)],
        ['Mai', parseInt(mai)],
        ['June', parseInt(jun)],
        ['July', parseInt(jul)],
        ['August', parseInt(aug)],
        ['September', parseInt(sep)],
        ['October', parseInt(oct)],
        ['November', parseInt(nov)],
        ['December', parseInt(dec)],
    ]);
    
    var data6 = google.visualization.arrayToDataTable([
        ['Roomtype', 'Singe', 'Double', 'Couple', 'Apartment', { role: 'annotation' } ],
        ['jan', parseInt(jan[0]), parseInt(jan[1]), parseInt(jan[2]), parseInt(jan[3]), ''],
        ['feb', parseInt(feb[0]), parseInt(feb[1]), parseInt(feb[2]), parseInt(feb[3]), ''],
        ['mar', parseInt(mar[0]), parseInt(mar[1]), parseInt(mar[2]), parseInt(mar[3]), ''],
        ['apr', parseInt(apr[0]), parseInt(apr[1]), parseInt(apr[2]), parseInt(apr[3]), ''],
        ['mai', parseInt(mai[0]), parseInt(mai[1]), parseInt(mai[2]), parseInt(mai[3]), ''],
        ['jun', parseInt(jun[0]), parseInt(jun[1]), parseInt(jun[2]), parseInt(jun[3]), ''],
        ['jul', parseInt(jul[0]), parseInt(jul[1]), parseInt(jul[2]), parseInt(jul[3]), ''],
        ['aug', parseInt(aug[0]), parseInt(aug[1]), parseInt(aug[2]), parseInt(aug[3]), ''],
        ['sep', parseInt(sep[0]), parseInt(sep[1]), parseInt(sep[2]), parseInt(sep[3]), ''],
        ['oct', parseInt(oct[0]), parseInt(oct[1]), parseInt(oct[2]), parseInt(oct[3]), ''],
        ['nov', parseInt(nov[0]), parseInt(nov[1]), parseInt(nov[2]), parseInt(nov[3]), ''],
        ['dec', parseInt(dec[0]), parseInt(dec[1]), parseInt(dec[2]), parseInt(dec[3]), ''],
        
      ]);
    var options = { 'title': 'Yearly reservations 2016', };
    var chart5 = new google.visualization.ColumnChart(document.getElementById('chart5'));
    chart5.draw(data6, options);
}
