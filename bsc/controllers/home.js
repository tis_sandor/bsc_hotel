

module.exports.controller = function(app) {

    app.get('/', function(req, res) {
        res.render('index', {
            title: 'Express',
            user: req.session.email
        });
    });

    app.get('/rooms', function(req, res) {
        res.render('rooms', {
            title: 'Express',
            user: req.session.email
        });
    });
}