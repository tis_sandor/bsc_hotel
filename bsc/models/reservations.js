var method = reservations.prototype;
var _ = require("lodash");

function reservations(client) {
    this.client = client;
};

var dateToInt = function(date) {
    return Math.floor(date / (1000 * 60 * 60 * 24));
};

var intToDate = function(num) {
    num = num * 1000 * 60 * 60 * 24;
    return new Date(num);
}

method.roomsInRange = function(start, end, roomtype, callback) {
    start = dateToInt(new Date(start));
    end = dateToInt(new Date(end));
    var range = _.range(start, end);
    range.push(roomtype);
    this.client.sinter(range, callback);
};

method.reserveRoom = function(start, end, room, rtype, email, callback) {
    start = dateToInt(new Date(start));
    end = dateToInt(new Date(end));
    this.client.watch(_.range(start, end));
    var trx = this.client.multi();
    var days = _.range(start, end);
    for(var i=0; i < days.length; i++) {
        trx.srem(days[i], room);
    }
    var key = "R:".concat() + parseInt(start) + ':' + parseInt(end) + ':' + email + ':' + rtype;
    console.log(key);
    trx.hmset([key, 'room', room]);
    console.log('trying to exec');
    trx.exec(callback);
};

method.cancelReservation = function(res_id, callback) {
    var start = parseInt(res_id.split(':')[2]);
    var end = parseInt(res_id.split(':')[3]);
    // var range = _.range(start, end);
    console.log('Dates to reinsert rooms:', start, ' ', end);
    var trx = this.client.multi();
    var id = res_id.slice(1);
    this.client.hmget([id, 'room'], function(err, res) {
        for (var i = start; i <= end; i++) {
            trx.sadd([i.toString(), res[0]]);
        }
        trx.del(id);
        trx.exec(callback);
    });
}

method.activateReservation = function(res_id, callback) {
    start = res_id.split(':')[1];
    end = res_id.split(':')[2];
    var range = _.range(start, end);
    var trx = this.client.multi();
    var id = res_id.slice(1);
    this.client.rename([id, "A" + id.slice(1)], callback);
}

method.closeReservation = function(res_id, callback) {
    start = res_id.split(':')[1];
    end = res_id.split(':')[2];
    var range = _.range(start, end);
    var trx = this.client.multi();
    var id = res_id.slice(1);
    this.client.rename([id, "B" + id.slice(1)], callback);
}

method.getActives = function(callback) {
    this.client.keys("A:*", callback);
}

method.getReservations = function(callback) {
    this.client.keys("R:*", callback);
}

method.getApStats = function(callback) {
    day = dateToInt(new Date());
    this.client.sinter([day, 'apartment'], function(err, res) {
        console.log(res);
        callback(res);
    });
}

method.getSingleStats = function(callback) {
    day = dateToInt(new Date());
    this.client.sinter([day, 'single'], function(err, res) {
        console.log(res);
        callback(res);
    });
}

method.getCoupleStats = function(callback) {
    day = dateToInt(new Date());
    this.client.sinter([day, 'couple'], function(err, res) {
        console.log(res);
        callback(res);
    });
}

method.getDoubleStats = function(callback) {
    day = dateToInt(new Date());
    this.client.sinter([day, 'double'], function(err, res) {
        console.log(res);
        callback(res);
    });
}
method.getYearStats = function(callback) {
    this.client.keys("*:*", function(err, res) {
        // console.log(res);
        var cal_s = {
            jan: 0,
            feb: 0,
            mar: 0,
            apr: 0,
            mai: 0,
            jun: 0,
            jul: 0,
            aug: 0,
            sep: 0,
            oct: 0,
            nov: 0,
            dec: 0,
        }
        var cal_d = {
            jan: 0,
            feb: 0,
            mar: 0,
            apr: 0,
            mai: 0,
            jun: 0,
            jul: 0,
            aug: 0,
            sep: 0,
            oct: 0,
            nov: 0,
            dec: 0,
        }
        var cal_c = {
            jan: 0,
            feb: 0,
            mar: 0,
            apr: 0,
            mai: 0,
            jun: 0,
            jul: 0,
            aug: 0,
            sep: 0,
            oct: 0,
            nov: 0,
            dec: 0,
        }
        var cal_a = {
            jan: 0,
            feb: 0,
            mar: 0,
            apr: 0,
            mai: 0,
            jun: 0,
            jul: 0,
            aug: 0,
            sep: 0,
            oct: 0,
            nov: 0,
            dec: 0,
        }
        var valid_date = dateToInt(new Date(2016, 0, 1))
        var valid_items = [];
        for (var i = 0; i < res.length; i++) {
            var item = parseInt(res[i].substr(2, 6));
            if (item >= valid_date) {
                var valid_item = parseInt(res[i].substr(2, 6));
                valid_item = intToDate(valid_item);
                var item_arr = res[i].split(':');
                if (item_arr[item_arr.length - 1] === 'single') {
                    switch (valid_item.getMonth()) {
                    case 0:
                        cal_s.jan += 1;
                        break;
                    case 1:
                        cal_s.feb += 1;
                        break;
                    case 2:
                        cal_s.mar += 1;
                        break;
                    case 3:
                        cal_s.apr += 1;
                        break;
                    case 4:
                        cal_s.mai += 1;
                        break;
                    case 5:
                        cal_s.jun += 1;
                        break
                    case 6:
                        cal_s.jul += 1;
                        break
                    case 7:
                        cal_s.aug += 1;
                        break;
                    case 8:
                        cal_s.sep += 1;
                        break;
                    case 9:
                        cal_s.oct += 1;
                        break;
                    case 10:
                        cal_s.nov += 1;
                        break;
                    case 11:
                        cal_s.dec += 1;
                        break;
                    }
                }
                else 
                if (item_arr[item_arr.length - 1] === 'double') {
                    switch (valid_item.getMonth()) {
                    case 0:
                        cal_d.jan += 1;
                        break;
                    case 1:
                        cal_d.feb += 1;
                        break;
                    case 2:
                        cal_d.mar += 1;
                        break;
                    case 3:
                        cal_d.apr += 1;
                        break;
                    case 4:
                        cal_d.mai += 1;
                        break;
                    case 5:
                        cal_d.jun += 1;
                        break
                    case 6:
                        cal_d.jul += 1;
                        break
                    case 7:
                        cal_d.aug += 1;
                        break;
                    case 8:
                        cal_d.sep += 1;
                        break;
                    case 9:
                        cal_d.oct += 1;
                        break;
                    case 10:
                        cal_d.nov += 1;
                        break;
                    case 11:
                        cal_d.dec += 1;
                        break;
                    }
                }
                else
                if (item_arr[item_arr.length - 1] === 'couple') {
                    switch (valid_item.getMonth()) {
                    case 0:
                        cal_c.jan += 1;
                        break;
                    case 1:
                        cal_c.feb += 1;
                        break;
                    case 2:
                        cal_c.mar += 1;
                        break;
                    case 3:
                        cal_c.apr += 1;
                        break;
                    case 4:
                        cal_c.mai += 1;
                        break;
                    case 5:
                        cal_c.jun += 1;
                        break
                    case 6:
                        cal_c.jul += 1;
                        break
                    case 7:
                        cal_c.aug += 1;
                        break;
                    case 8:
                        cal_c.sep += 1;
                        break;
                    case 9:
                        cal_c.oct += 1;
                        break;
                    case 10:
                        cal_c.nov += 1;
                        break;
                    case 11:
                        cal_c.dec += 1;
                        break;
                    }
                }
                else
                if (item_arr[item_arr.length - 1] === 'apartment') {
                    switch (valid_item.getMonth()) {
                    case 0:
                        cal_a.jan += 1;
                        break;
                    case 1:
                        cal_a.feb += 1;
                        break;
                    case 2:
                        cal_a.mar += 1;
                        break;
                    case 3:
                        cal_a.apr += 1;
                        break;
                    case 4:
                        cal_a.mai += 1;
                        break;
                    case 5:
                        cal_a.jun += 1;
                        break
                    case 6:
                        cal_a.jul += 1;
                        break
                    case 7:
                        cal_a.aug += 1;
                        break;
                    case 8:
                        cal_a.sep += 1;
                        break;
                    case 9:
                        cal_a.oct += 1;
                        break;
                    case 10:
                        cal_a.nov += 1;
                        break;
                    case 11:
                        cal_a.dec += 1;
                        break;
                    }
                }
            }
        }
        console.log(cal_s);
        callback(cal_s, cal_d, cal_c, cal_a);
    });
}

module.exports = reservations;